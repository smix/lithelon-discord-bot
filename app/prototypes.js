const util = require('util');
const {get} = require('lodash');

String.prototype.format = function () {
    const args = Array.prototype.slice.call(arguments);
    const value = this.toString();
    args.unshift(value);
    return util.format.apply(util, args);
};

Array.prototype.get = function (path, defaults) {
    return get(this, path, defaults);
};