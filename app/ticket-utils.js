const config = require('../configs/tickets');

class TicketUtils {
    static isClaimer(channel, user, cache) {
        if (cache == undefined) return false;
        if (channel.parent.id == config.parent_category) return cache.claim_user_id == user.id;
        return false;
    }
}

module.exports = TicketUtils;