const fs = require('fs');
const path = require('path');
const {get, remove} = require('lodash');

class TicketData {
    constructor() {
        this.data = this.read();
    }

    save() {
        const raw = JSON.stringify(this.data, null, 4);
        fs.writeFileSync(this.dataPath, raw);
    }

    read() {
        const raw = fs.readFileSync(this.dataPath);
        return JSON.parse(raw);
    }

    addCache(data) {
        this.data.cache.push(data);
        this.save();
    }

    findCache(path, value) {
        return this.data.cache.find(function (item) {
            return get(item, path) == value;
        });
    }

    removeCache(path, value) {
        remove(this.data.cache, function (item) {
            return get(item, path) == value;
        });
        this.save();
    }

    get dataPath() {
        return path.join(process.cwd(), 'ticket-data.json');
    }
}

module.exports = new TicketData;