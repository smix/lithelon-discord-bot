module.exports = {
    prefix: '~',
    token: process.env.DISCORD_TOKEN,
    paste_key: process.env.PASTE_KEY,
    discord: {
        partials: ['CHANNEL', 'MESSAGE', 'REACTION'],
        presence: {
            activity: {
                type: 'PLAYING',
                name: 'Minecraft'
            }
        }
    }
};