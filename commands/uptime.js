const moment = require('moment');
const BaseCommand = require('./base');

module.exports = new BaseCommand({
    name: 'uptime',
    description: 'Shows the bots uptime.',
    execute(message) {
        const uptime = moment.utc(message.client.uptime).format('HH:mm');
        message.reply(uptime);
    }
});