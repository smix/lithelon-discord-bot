const {divide} = require('lodash');
const {Collection} = require('discord.js');
const config = require('../configs/app');

class Command {
    constructor(options) {
        this.name = options.name;
        this.usage = options.usage || '';
        this.description = options.description;
        this.args = options.args || 0;
        this.cooldown = options.cooldown || 5;
        this.aliases = options.aliases || new Array;
        this.roles = options.roles || new Array;
        this.execute = options.execute;
        this.prefix = config.prefix;
    }

    process(message, args) {
        if (this.hasRoles(message) == false) {
            return message.reply('No permission for that command.');
        }
        const seconds = this.onCooldown(message).toFixed(0);
        if (seconds > 0) {
            return message.reply(`Please wait ${seconds} seconds before using command.`);
        }
        if (this.isMissingArgs(args)) {
            return message.reply(this.usageDisplay);
        }
        this.execute(message, args);
    }

    onCooldown(message) {
        const now = Date.now();
        const {author, client} = message;
        const timestamps = client.cooldowns.get(this.name);
        if (timestamps.has(author.id)) {
            const milliseconds = this.cooldown * 1000;
            const expiration = timestamps.get(author.id) + milliseconds;
            if (now < expiration) return divide(expiration - now, 1000);
        }
        timestamps.set(author.id, now);
        return 0;
    }

    isMissingArgs(args) {
        if (this.args == 0) return false;
        return args.length == 0 || args.length < this.args;
    }

    hasRoles(message) {
        if (this.roles.length == 0) return true;
        return this.roles.some(function (id) {
            return message.member.roles.cache.has(id);
        });
    }

    register(client) {
        const {commands, cooldowns} = client;
        commands.set(this.name, this);
        cooldowns.set(this.name, new Collection);
    }

    get usageDisplay() {
        return `${this.prefix}${this.name} ${this.usage}`;
    }
}

module.exports = Command;