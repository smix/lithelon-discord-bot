const puppeteer = require('puppeteer');
const BaseCommand = require('./base');
const config = require('../configs/voting');

module.exports = new BaseCommand({
    name: 'votebump',
    description: 'Bump our vote site.',
    cooldown: 60,
    execute(message) {
        bumpServer(message);
    }
});

async function bumpServer(message) {
    const browser = await puppeteer.launch(config.puppeteer);
    const page = await browser.newPage();
    await page.goto('https://minecraft-server.net/user_cpl');
    await page.type(config.selectors.username, config.username);
    await page.type(config.selectors.password, config.password);
    await page.click('button:nth-child(1)');
    await page.waitFor(1500);
    page.content().then(console.log);
    try {
        const button = await page.$eval('button:nth-child(1)', elem => elem.textContent);
        console.log(button);
        return message.reply(`Bump again in ${button.substr(12)}`);
    } catch (error) {
        console.log('Assuming vote site needs a bump.', error.message);
        await page.click('input[name=bump]');
        return message.reply('Vote site has been bumped.');
    } finally {
        await browser.close();
    }
}