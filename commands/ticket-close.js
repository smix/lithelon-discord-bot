const os = require('os');
const moment = require('moment');
const {MessageEmbed} = require('discord.js');
const BaseCommand = require('./base');
const TicketUtils = require('../app/ticket-utils');
const Paste = require('../app/paste');
const config = require('../configs/tickets');
const persist = services.ticketPersist;

module.exports = new BaseCommand({
    name: 'ticketclose',
    description: 'Closes the ticket.',
    usage: '<reason>',
    args: 1,
    execute(message, args) {
        const {channel, author, guild} = message;
        const cache = persist.findCache('channel_id', channel.id);
        if (TicketUtils.isClaimer(channel, author, cache) == false) return;
        channel.messages.fetch().then(function (messages) {
            let trans = new Array;
            messages.forEach(function (item) {
                const created = moment(item.createdTimestamp).format('MM-DD-YYYY HH:mm:ss z');
                trans.push(`${item.member.displayName} ${created}`);
                trans.push(item.cleanContent);
                trans.push(null);
            });
            trans = trans.join(os.EOL);
            Paste.paste('Transcript', trans).then(function (resp) {
                console.log(resp.data.link);
                const actions = guild.channels.cache.get(config.action_channel_id);
                const embed = new MessageEmbed();
                embed.setDescription('A ticket was closed. The transcript will expire in 4 weeks.');
                embed.addField('Created By', cache.user_create_name);
                embed.addField('Closed By', author.username);
                embed.addField('Close Reason', args.join(' '));
                embed.addField('Transcript', resp.data.link);
                actions.send(embed).then(function () {
                    channel.delete();
                });
            });
        }).catch(console.error);
    }
});